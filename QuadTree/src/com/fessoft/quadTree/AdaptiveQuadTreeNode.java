package com.fessoft.quadTree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


public class AdaptiveQuadTreeNode extends QuadTreeNode {
	public static final int MERGE_TRESHOLD 	= 8;
	public static final int SUBDIVIDE_TRESHOLD = 24;
		
	private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	
	private QuadTreeNode[] nodes = null;
	private ArrayList<QuadTreeElement> elements = new ArrayList<QuadTreeElement>();
		
	protected AdaptiveQuadTreeNode(QuadTreeNode parent, int x1, int y1, int x2, int y2) {
		super(parent, x1, y1, x2, y2);
	}
	
	protected void __insert(QuadTreeElement element) {
		if(elements != null) {
			if(elements.size() >= SUBDIVIDE_TRESHOLD -1) {
				split();
			}else{
				element.setParent(this);
				elements.add(element);
				
				unlockForWriting();
				return;
			}
		}
		
		if(elements == null) {
			QuadTreeNode node;
			if(element.getY() < pivotY) {
				if(element.getX() < pivotX) {
					node = nodes[0];
				}else{
					node = nodes[1];
				}
			}else{
				if(element.getX() < pivotX) {
					node = nodes[2];
				}else{
					node = nodes[3];
				}
			}
			
			node.lockForWriting();
			this.unlockForWriting();
			node.__insert(element);
		}
	}
	
	protected void __query(int minX, int minY, int maxX, int maxY, ArrayList<QuadTreeElement> elementsFound) {	
		if(!(minX > this.maxX || maxX < this.minX || minY > this.maxY || maxY < this.minY)) {
			if(elements != null) {
				for(QuadTreeElement element : elements) {
					int x,y;
					synchronized(element) {
						x = element.getX();
						y = element.getY();
					}
					
					if(		x >= minX && x <= maxX &&
							y >= minY && y <= maxY) {
						elementsFound.add(element);
					}
				}
				
				this.unlockForReading();
			}else{
				QuadTreeNode[] nodes = this.nodes;
				
				for(QuadTreeNode node : nodes) {
					node.lockForReading();
				}
				
				this.unlockForReading();
				
				for(QuadTreeNode node : nodes) {
					node.__query(minX, minY, maxX, maxY, elementsFound);
				}
			}
		}else{
			this.unlockForReading();
		}
	}
	
	protected void remove(QuadTreeElement element) {
		elements.remove(element);
		unlockForWriting();
		
		maybeMerge();
	}
	
	protected void update(QuadTreeElement element, int newX, int newY) {		
		if(this.elements != null) {
			element.set(newX, newY, this);
			unlockForWriting();
		}else{
			int x,y;
			synchronized(element) {
				x = element.getX();
				y = element.getY();
			}
			
			int[][] positions = new int[2][2];
			positions[0][0] = x;
			positions[0][1] = y;
			positions[1][0] = newX;
			positions[1][1] = newY;
			
			ArrayList<QuadTreeNode> locked = new ArrayList<QuadTreeNode>();
			deepSelectiveWriteLock(positions, locked);
			
			element.remove();
			element.set(newX, newY);
			__insert(element);
			
			for(QuadTreeNode lockedNode : locked) {
				lockedNode.unlockForWriting();
			}
		}
	}
	
	public void maybeMerge() {
		boolean maybeMergeDown = false;
		
		lockForReading();
		if(!hasCountOf(MERGE_TRESHOLD)) {
			unlockForReading();
			lockForWriting();
			if(!hasCountOf(MERGE_TRESHOLD)) {
				merge();
				maybeMergeDown = true;
			}
			unlockForWriting();
		}else{
			unlockForReading();
		}
		
		if(maybeMergeDown && parent != null) {
			parent.maybeMerge();
		}
	}
	
	private void split() {		
		ArrayList<QuadTreeElement> elements = this.elements;
		this.elements = null;
		nodes = new AdaptiveQuadTreeNode[4];
		nodes[0] = new AdaptiveQuadTreeNode(this, minX, 	minY, 		pivotX, 	pivotY);
		nodes[1] = new AdaptiveQuadTreeNode(this, pivotX, 	minY, 		maxX, 		pivotY);
		nodes[2] = new AdaptiveQuadTreeNode(this, minX, 	pivotY, 	pivotX, 	maxY);
		nodes[3] = new AdaptiveQuadTreeNode(this, pivotX, 	pivotY, 	maxX, 		maxY);
		
		for(QuadTreeElement element : elements) {
			lockForWriting();
			__insert(element);
		}
	}
	
	private void merge() {
		deepWriteLock();
		
		HashSet<QuadTreeElement> elements = new HashSet<QuadTreeElement>();
		getAllElements(elements);
		
		QuadTreeNode[] oldNodes = nodes;
		nodes = null;
		this.elements = new ArrayList<QuadTreeElement>();
		
		for(QuadTreeElement element : elements) {
			lockForWriting();
			__insert(element);
		}
		
		nodes = oldNodes;
		deepWriteUnLock();
		nodes = null;
	}
	
	protected void getAllElements(HashSet<QuadTreeElement> elements) {
		if(this.elements != null) {
			for(QuadTreeElement element : this.elements) {
				elements.add(element);
			}
		}else{
			for(QuadTreeNode node : this.nodes) {
				node.getAllElements(elements);
			}
		}
	}
	
	protected boolean hasCountOf(int count) {
		return __hasCountOf(count) >= count;
	}
	
	protected int __hasCountOf(int count) {
		lockForReading();
		try {
			if(elements != null) {
				return elements.size();
			}else{
				int totalCount = 0;
				for(QuadTreeNode node : nodes) {
					totalCount += node.__hasCountOf(count);
					
					if(totalCount >= count) {
						return totalCount;
					}
				}
				return totalCount;
			}
		}finally{
			unlockForReading();
		}
	}
	
	protected void deepWriteLock() {
		lockForWriting();

		if(nodes != null) {
			for(QuadTreeNode node : nodes) {
				node.deepWriteLock();
			}
		}
	}
	
	protected void deepWriteUnLock() {		
		if(nodes != null) {
			for(QuadTreeNode node : nodes) {
				node.deepWriteUnLock();
			}
		}
		
		unlockForWriting();
	}
	
	protected void deepSelectiveWriteLock(int[][] positions, ArrayList<QuadTreeNode> locked) {
		boolean positionMatch = false;
		for(int[] position :  positions) {
			if(this.containsFully(position[0], position[1], position[0], position[1])) {
				positionMatch = true;
				break;
			}
		}
		
		if(positionMatch) {
			lockForWriting();
			locked.add(this);
			if(elements == null) {
				for(QuadTreeNode node : nodes) {
					node.deepSelectiveWriteLock(positions, locked);
				}
			}
		}
	}
		
	public void printDebugData(String ident) {
		System.out.println(ident+""+this);
		if(elements != null) {
			int i=1;
			for(QuadTreeElement element : elements) {
				System.out.println(ident+"\t"+(i++)+". "+element);
			}
		}else{
			for(QuadTreeNode node : nodes) {
				node.printDebugData(ident+"\t");
			}			
		}
	}
		
	protected void lockForReading() {	
		addLockCount(lock.readLock());
		lock.readLock().lock();
	}
	
	protected void unlockForReading() {
		reduceLockCount(lock.readLock());	
		lock.readLock().unlock();
	}
	
	protected void lockForWriting() {
		addLockCount(lock.writeLock());
		lock.writeLock().lock();
	}
	
	protected void unlockForWriting() {
		reduceLockCount(lock.writeLock());	
		lock.writeLock().unlock();
	}
	
	private static ThreadLocal<HashMap<Lock, Integer>> openLocks = new ThreadLocal<HashMap<Lock, Integer>>() {
		protected HashMap<Lock, Integer> initialValue() {
            return new HashMap<Lock, Integer>();
		}		
	};
	
	private static ThreadLocal<ArrayList<LockLog>> lockLocations = new ThreadLocal<ArrayList<LockLog>>() {
		protected ArrayList<LockLog> initialValue() {
            return new ArrayList<LockLog>();
		}		
	};
	
	private static void addLockCount(Lock lock) {
		if(QuadTree.DEBUG) {
			lockLocations.get().add(new LockLog(lock, true));
			Integer count = openLocks.get().get(lock);
			if(count == null) {
				openLocks.get().put(lock, 1);
			}else{
				openLocks.get().put(lock, count+1);
			}
		}
	}
	
	private static void reduceLockCount(Lock lock) {
		if(QuadTree.DEBUG) {
			lockLocations.get().add(new LockLog(lock, false));
			Integer count = openLocks.get().get(lock);
			if(count == 1) {
				openLocks.get().remove(lock);
			}else{
				openLocks.get().put(lock, count-1);
			}
		}
	}
	
	public static void checkLocks() {
		if(QuadTree.DEBUG) {
			if(openLocks.get().size() > 0) {			
				for(LockLog lockLog : lockLocations.get()) {
					if(lockLog.isLock) {
						System.err.println(" +  "+lockLog.lock);
					}else{
						System.err.println(" -  "+lockLog.lock);
					}
					
					int position =0;
					for(StackTraceElement ste : lockLog.elements) {
						if(position >3 && position<10) {
							System.err.println("    "+ste.getFileName()+":"+ste.getLineNumber());
						}
						position++;
					}
				}
				
				System.err.println(" ----- You hava locks!! "+openLocks.get()+" -----");
				openLocks.get().clear();
			}
			lockLocations.get().clear();
		}
	}
	
	public String toString() {
		return "("+minX+", "+minY+") ("+maxX+", "+maxY+")";
	}
	
	private static class LockLog {
		public final boolean isLock;
		public final Lock lock;
		public final StackTraceElement[] elements;
		
		public LockLog(Lock lock, boolean isLock) {
			this.lock = lock;
			this.isLock = isLock;
			this.elements = Thread.currentThread().getStackTrace();
		}
	}
}
