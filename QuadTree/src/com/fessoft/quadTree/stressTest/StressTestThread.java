package com.fessoft.quadTree.stressTest;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import com.fessoft.quadTree.QuadTree;

public class StressTestThread extends Thread {
	
	private int elementCount;
	private QuadTree tree;
	
	public int actionCounter = 0;
	
	public boolean running = true;
	
	public StressTestThread(String name, QuadTree tree, int elementCount) {
		super(name);
		this.tree = tree;
		this.elementCount = elementCount;
	}
	
	private ArrayList<StressTestElement> elements = new ArrayList<StressTestElement>(); 
	
	public void run() {
		ThreadLocalRandom r = ThreadLocalRandom.current();
		
		for(int i=0;i<elementCount;i++) {
			StressTestElement testElement = new StressTestElement();
			testElement.randomPosition(r);
			elements.add(testElement);
			tree.insert(testElement);
		}

		int tinyCount =0;
		while(running) {
			tinyCount++;
			if(tinyCount == 100) {
				synchronized(this) {
					actionCounter += 100;
				}
				tinyCount = 0;
			}
			
			int randomInt = r.nextInt(10000);
			if(randomInt == 0){
				elements.get(r.nextInt(elementCount)).randomPosition(r);
			} else if(randomInt == 1){
				int elementPosition = r.nextInt(elementCount);
				StressTestElement element = elements.get(elementPosition);
				element.doRemove();
				
				element = new StressTestElement();
				element.randomPosition(r);
				elements.set(elementPosition, element);
				tree.insert(element);
			}else if(randomInt == 2) {
				int elementPosition = r.nextInt(elementCount);
				StressTestElement element = elements.get(elementPosition);
				
				int x,y;
				synchronized(element) {
					x = element.getX();
					y = element.getY();
				}
				
				if(!tree.query(x, y, x, y).contains(element)) {
					System.out.println("-----");
					System.out.println("SEARCHING FOR: "+element);
					System.out.println("-----");
					tree.printDebugData("");
					throw new RuntimeException("Query for own element failed");
				}
			}else if(randomInt < 1000) {
				int elementPosition = r.nextInt(elementCount);
				StressTestElement element = elements.get(elementPosition);
				
				if(!element.getNeighbours(r.nextInt(10000), r.nextInt(10000)).contains(element)) {
					System.out.println("-----");
					System.out.println("SEARCHING FOR: "+element);
					System.out.println("-----");
					tree.printDebugData("");
					
					throw new RuntimeException("Query for own element failed");
				}
			}else{
				elements.get(r.nextInt(elementCount)).moveABit(r);
			}
		}
	}
	
	public void stopMe() {
		running = false;
	}
	
	public int getAndResetActionCounter() {
		synchronized(this) {
			int temp = actionCounter;
			actionCounter = 0;
			return temp;
		}
	}
	
}
