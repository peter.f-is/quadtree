package com.fessoft.quadTree.stressTest;

import java.util.ArrayList;

import com.fessoft.quadTree.QuadTree;

public class StressTest {
	public static void main(String[] args) {			
		//int RUNTIME = 30000;
		//int[] TOTAL_ELEMENTS_ARRAY = new int[]{1000,10000,100000,1000000};
		//int[] THREADS_ARRAY = new int[]{1,5,10,25,50,100,250,1000};
		//int RUNS = 3;
		
		int RUNTIME = 300000;
		int[] TOTAL_ELEMENTS_ARRAY = new int[]{25000};
		int[] THREADS_ARRAY = new int[]{10};
		int RUNS = 1000;
		
		for(int i=1;i<=RUNS;i++) {
			System.out.println("------- START OF ITERATION "+i+" -------");
			for(int TOTAL_ELEMENTS : TOTAL_ELEMENTS_ARRAY) {
				for(int THREADS : THREADS_ARRAY) {
					long tps = stress(RUNTIME, THREADS, TOTAL_ELEMENTS/THREADS);
					
					System.out.println(THREADS +","+TOTAL_ELEMENTS+","+tps);
					
					System.gc();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {}
					System.gc();
				}
			}
		}
	}
	
	public static long stress(int RUNTIME, int THREADS, int ELEMENT_PER_THREAD) {
		final QuadTree tree = new QuadTree();
		
		ArrayList<StressTestThread> threads = new ArrayList<StressTestThread>();
		for(int i=0;i<THREADS;i++) {
			StressTestThread thread = new StressTestThread("StressTestThread "+i, tree, ELEMENT_PER_THREAD);
			thread.start();
			threads.add(thread);
		}
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {}
		
		long lastTime = System.currentTimeMillis();
		long totalTime = 0;
		while(true) {
			long actions = 0;
			for(StressTestThread thread : threads) {
				actions += thread.getAndResetActionCounter();
			}
			
			long myLastTime = lastTime;
			long myEndTime = System.currentTimeMillis();
			lastTime = myEndTime;
			long time = myEndTime - myLastTime;
			
			totalTime += time;
			
			if(time > 0) {
				//System.out.println("Transactions/second: "+ ((actions * 1000) / time));
			}
			
			if(totalTime > RUNTIME && time > 9000) {
				for(StressTestThread thread : threads) {
					thread.stopMe();
				}
				
				return ((actions * 1000) / time);
			}
			
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {}
		}
	}
}
